---
marp: true
theme: gaia
#template: gaia
#size: 4:3
size: 16:9
paginate: false
class:
- gaia
#- invert
- lead
# header: 'Header content'
# footer: 'Footer content'
# https://github.com/marp-team/marp-core/tree/master/themes#readme
# https://gist.github.com/yhatt/a7d33a306a87ff634df7bb96aab058b5

---
# Kubecon & CloudNativeCon EU 2020 virtual
---

## Lightning talks I

 * Why you should use PDBs
 * Extend envoy: use WebAssembly
    * AssembleScript - feels like Typescript
 * SOPS, Secrets and CI pipeline for secure configuration
  * why not vault?
    * Because it requires a runtime
    * complex and expensive to manage
  * SOPS supports GPG, GCP KMS, ...


---

## Lightning talks II

 * BUILD AND DEPLOY CNF in 5 minutes
   * Cloud-native Network Funtion
    * fd.io, ligato.io, networksevicemesh.io
 * New Application Spec

---
### Cloud Native Security Tutorial
![bg 70% left](./kubesec-cover.png)

 * Recommended book with more details
 * Tutorial is online: https://tutorial.kubernetes-security.info

 ---

 * deploy a compromised pod with shellshock
 * use image scanner to detect this vulnerability in CI/CD
 * define policies to prevent some cluster workloads and settings
 * use GitOps to ensure policies

---

#### image scanning

 * detect all known bugs in operating system and libraies
 * scan image in multiple stages
   * CI: block MR with known bugs (e.g. [trivy](https://github.com/aquasecurity/trivy))
   * Deployment: scan when deployment/pod is created ([OPA](https://www.openpolicyagent.org/))
   * Runtime: check deployed images regulary and alert (e.g. [starboard](https://github.com/aquasecurity/starboard))

---
### scan results with trivy

using latest master images
```
## java/dropwizard services
eu.gcr.io/development-666/guinea-pig (debian 10.3)
===========================================================================================
Total: 99 (UNKNOWN: 0, LOW: 73, MEDIUM: 25, HIGH: 1, CRITICAL: 0)

## web-frontend & login-service
eu.gcr.io/development-666/web-frontend (alpine 3.11.6)
===============================================================================================
Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 1, HIGH: 0, CRITICAL: 0)

## backoffice
eu.gcr.io/development-666/backoffice-master (debian 9.13)
==================================================================================================
Total: 3603 (UNKNOWN: 0, LOW: 3054, MEDIUM: 431, HIGH: 118, CRITICAL: 0)

```
---
# prevent security issues

 * use tools like **O**pen**P**olicy**A**gent as k8s admission controller
 * define policies to allow or deny cluster changes
   * deny exposing service publicly (if not allowlisted)
   * deny HTTP requests without valid JWT signature
   * ensure image is from trusted registry
   * ...
---
# Why GitOps?

* no direct (write) access to the cluster for developers
* the option is to use git
* humans & bots will accept changes via git commits and a agent applies it
* very clear boundaries
* clear who requessted and reviewed the change
* easy to enforce best practices and policies


---
# GKE security

![img](./sec_levels.png)

---

## workload security (pod/container)

 * assume you will be owned: use distroless images
 * easy reuild of images
 * sign your images
 * disable default k8s ServiceAccount. Most containers don't need it
 * define PodSecurityPolicies

---

## cluster security (API-Server and Nodes)

 * Keep the cluster up to date (auto-update patch releases)
 * Isolate the cluster from the internet (no NodePorts or public IPs)
 * Use secrets for secrets and store them only encrypted in git

---

## user security

 * use RBAC and groups to bind access roles to groups
 * manage users in groups for easy on/offboarding
 * use policy agents to protect resources
 * more details: [Help! My Cluster Is On The Internet!](https://docs.google.com/document/d/1OefvSpURuOHdNNiwbRXmhPiRUFsYnllgMPDaVqXRkh4/edit#heading=h.lroslfbjn64r)


---


![bg contain](./recap.png)